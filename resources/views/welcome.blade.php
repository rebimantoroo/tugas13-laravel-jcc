@extends('adminlte.master')

@section('judulFile')
  Dashboard
@endsection

@section('judul')
  Selamat Datang di Halaman Dashboard
@endsection


@section('isi')
    <h3 align="center"> Silahkan Pilih Table</h3>
    <div class="row">
      <div class="col-6">
        <div class="small-box bg-info">
          <div class="inner">
            <h3>Table</h3>
            <p></p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="/table" class="small-box-footer">Menuju halaman Table <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h3>Data-Tables</h3>
            <p></p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="/data-tables" class="small-box-footer">Menuju halaman Data-Tables <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>

    </div>

@endsection
